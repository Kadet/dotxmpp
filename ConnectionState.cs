﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP
{
    public enum ConnectionState
    {
        Disconnected,
        Connecting,
        Connected,
        Securing,
        secured,
        Authorising,
        Authorised,
        Unauthorised,
        Binding,
        Binded,
        SessionStarting,
        SessionStarted,
        Running
    }
}
