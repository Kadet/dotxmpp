﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Protocol.Sasl
{
    class Success : XmlParser.Dom.Element
    {
        public Success() : base("success", "", URI.Sasl) { }
    }
}
