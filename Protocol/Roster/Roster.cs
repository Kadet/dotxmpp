﻿using System;
using System.Collections;
using System.Linq;
using System.Text;

namespace DotXMPP.Protocol.Roster
{
    public delegate void RosterItemHandler(object sender, RosterItem item);

    public class Roster
    {
        public Hashtable Items { get; private set; }
        private XmppConnection Conn { get; set; }

        public event EventHandler OnStart;
        public event RosterItemHandler OnItem;
        public event EventHandler OnGet;

        public Roster(XmppConnection conn)
        {
            Conn = conn;
            Items = new Hashtable();
        }

        public void Get()
        {
            var iq = new IQ.Iq();
            iq.Type = IQ.IQType.Get;
            iq.Id = "roster_" + Utils.Random.GetRandom(0, 10000);
            iq.Query = new RosterQuery();
            Conn.Write(iq);

            Conn.BeginWaitingForId(iq.Id, new AsyncCallback(onResult));
        }

        public void onResult(IAsyncResult res)
        {
            var result = Conn.EndWaiting(res) as IQ.Iq;

            if (result.Type == IQ.IQType.Result)
            {
                if (this.OnStart != null)
                    this.OnStart(this, EventArgs.Empty);

                var element = new RosterQuery(result.Query);

                foreach (var item in element.Items)
                    SetItem(item);

                if (this.OnGet != null)
                    this.OnGet(this, EventArgs.Empty);
            }
        }

        public void Add(Jid jid, string name, string[] groups)
        {
            var iq = new IQ.Iq();
            iq.Type = IQ.IQType.Set;
            iq.From = Conn.Jid.ToString();
            iq.Id = "roster_" + Utils.Random.GetRandom(0, 10000);
            var query = new RosterQuery();
            var item = new RosterItem(jid, name, groups);
            query.AddItem(item);
            iq.Query = query;
            Conn.Write(iq);

            SetItem(item);
        }

        public void Remove(Jid jid)
        {
            var iq = new IQ.Iq();
            iq.Type = IQ.IQType.Set;
            iq.From = Conn.Jid.ToString();
            iq.Id = "roster_" + Utils.Random.GetRandom(0, 10000);
            var query = new RosterQuery();
            var item = new RosterItem();
            item.Jid = jid;
            item.Subscripion = Presence.SubscriptionType.Remove;
            query.AddItem(item);
            iq.Query = query;
            Conn.Write(iq);

            RemoveItem(item);
        }

        public void Edit(Jid jid, string name, string[] groups)
        {
            var iq = new IQ.Iq();
            iq.Type = IQ.IQType.Set;
            iq.From = Conn.Jid.ToString();
            iq.Id = "roster_" + Utils.Random.GetRandom(0, 10000);
            var query = new RosterQuery();
            var item = new RosterItem(jid, name, groups);
            query.AddItem(item);
            iq.Query = query;
            Conn.Write(iq);

            SetItem(item);
        }

        private void SetItem(RosterItem item)
        {
            Items[item.Jid] = item;
            if (OnItem != null)
                OnItem(this, item);
        }

        private void RemoveItem(RosterItem item)
        {
            Items.Remove(item.Jid);
            if (OnItem != null)
                OnItem(this, item);
        }
    }
}
