﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XmlParser.Dom
{
    public class Element
    {
        #region << Properties >>
        /// <summary>
        /// Childs of element
        /// </summary>
        public List<Dom.Element> Childs { get; private set; }

        /// <summary>
        /// Attributes of element
        /// </summary>
        public Dictionary<string, string> Attributes { get; private set; }

        /// <summary>
        /// Content of element
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Name (tag) of element
        /// </summary>
        public string Tag { get; set; }

        /// <summary>
        /// Element parent
        /// </summary>
        public Dom.Element Parent { get; set; }

        /// <summary>
        /// Element Namespace
        /// </summary>
        public string NameSpace 
        {
            get 
            {
                var ns = this.GetAttribute("xmlns");

                if (ns == null && Parent != null)
                    ns = Parent.NameSpace;

                return ns;
            }
            set { this.SetAttribute("xmlns", value); }
        }

        /// <summary>
        /// Gets element as XML
        /// </summary>
        public string Xml
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<" + this.Tag);

                foreach (var attrib in Attributes)
                    sb.Append(" " + Escape(attrib.Key) + "=\"" + Escape(attrib.Value) + "\"");
                sb.Append(">");

                if (this.Childs.Count > 0)
                {
                    foreach (var child in Childs)
                        sb.Append(child.Xml);
                }
                else if(this.Content != null)
                    sb.Append(Escape(this.Content));
                sb.Append("</" + this.Tag + ">");

                return sb.ToString();
            }
        }
        #endregion
        #region << Constructors >>
        public Element()
        {
            this.Attributes = new Dictionary<string, string>();
            this.Childs = new List<Element>();
        }

        /// <summary>
        /// Creates new element with tag "tag"
        /// </summary>
        /// <param name="tag"></param>
        public Element(string tag)
            : this()
        {
            this.Tag = tag;
        }

        /// <summary>
        /// Creates new element with tag "tag" and content "content"
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="content"></param>
        public Element(string tag, string content)
            : this(tag)
        {
            this.Content = content;
        }

        /// <summary>
        /// Creates new element with tag "tag", content "content" and namespace "ns"
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="content"></param>
        /// <param name="ns"></param>
        public Element(string tag, string content, string ns)
            : this(tag, content)
        {
            this.NameSpace = ns;
        }

        public Element(Dom.Element obj)
        {
            FromElement(obj);
        }

        public Element(Dom.Element parent, string tag)
        {
            this.Parent = parent;
            this.Tag = tag;
        }

        public Element(Dom.Element parent, string tag, string content)
            : this(parent, tag)
        {
            this.Content = content;
        }

        public Element(Dom.Element parent, string tag, string content, string ns)
            : this(parent, tag, content)
        {
            this.NameSpace = ns;
        }
        #endregion
        #region << Child managing >>
        public void AddChild(Dom.Element child)
        {
            child.Parent = this;
            this.Childs.Add(child);
        }

        public void AddChild(string tag, string content)
        {
            this.Childs.Add(new Element(tag, content));
        }

        public List<Element> GetChilds(string tag)
        {
            List<Element> ret = new List<Element>();
            foreach (var child in this.Childs)
                if (child.Tag == tag)
                    ret.Add(child);
            return (ret.Count > 0 ? ret : null);
        }

        public Element Child(string tag, int index = 0)
        {
            var childs = GetChilds(tag);
            if(index < 0)
                throw new ArgumentOutOfRangeException("index");

            return (childs != null ? childs[index] : null);
        }

        public bool HasChild(string tag, int index = 0)
        {
            return Child(tag, index) != null;
        }

        public void SetChild(string tag, string content, int index = 0)
        {
            var child = Child(tag, index);
            if (child == null)
                AddChild(tag, content);
            else
                child.Content = content;
        }

        public void SetChild(string tag, Element element, int index = 0)
        {
            var child = Child(tag, index);
            if (child == null)
                AddChild(element);
            else
                this.Childs[Childs.IndexOf(child)] = element;
        }

        public void RemoveChild(string tag, int index = 0)
        {
            int count = 0;
            foreach (var child in this.Childs)
            {
                if (child.Tag == tag && count == index)
                    Childs.Remove(child);
                if (child.Tag == tag)
                    count++;
            }
        }
        #endregion
        #region << Attribute managing >>
        public void SetAttribute(string name, string value)
        {
            if (value == null && Attributes.ContainsKey(name))
                Attributes.Remove(name);
            else
                Attributes[name] = value;
        }

        public bool HasAttribute(string name)
        {
            return (Attributes[name] != null);
        }

        public string GetAttribute(string name)
        {
            if (this.Attributes.ContainsKey(name))
                return Attributes[name];
            else 
                return null;
        }

        public void RemoveAttribute(string name)
        {
            if (name != null && Attributes.ContainsKey(name))
                Attributes.Remove(name);
        }
        #endregion

        public void FromElement(Dom.Element obj)
        {
            this.Childs = obj.Childs;
            this.Attributes = obj.Attributes;
            this.Tag = obj.Tag;
            this.Parent = obj.Parent;
            this.Content = obj.Content;
        }

        public static string Escape(string strToEscape)
        {
            strToEscape = strToEscape.Replace("&", "&amp;");
            strToEscape = strToEscape.Replace("\"", "&quot;");
            strToEscape = strToEscape.Replace("'", "&apos;");
            strToEscape = strToEscape.Replace("<", "&lt;");
            strToEscape = strToEscape.Replace(">", "&gt;");
            return strToEscape;
        }
    }
}
