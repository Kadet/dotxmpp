﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Net.Sockets;
using System.Net;

using System.Timers;
using System.IO;

using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;

namespace DotXMPP.Net
{
    public class BaseSocket
    {
        public delegate void SocketEventHandler(object sender, string content);

        #region Private Members
        const int BUFFER_SIZE = 16384;

        private Queue<byte[]> WriteQueue = new Queue<byte[]>();
        private bool Pending = false;

        private byte[] Buffer = new byte[BUFFER_SIZE];
        private Stream Stream = null;

        private NetworkStream NetStream = null;
        private SslStream SslStream = null;

        private Socket m_Sock = null;
        private string m_Server;
        private int m_Port;
        private bool m_Ssl;

        private Timer KeepAliveTimer = null;
        #endregion

        #region Properties
        public string Server
        {
            get { return m_Server; }
            set { m_Server = value; }
        }

        public int Port
        {
            get { return m_Port; }
            set { m_Port = value; }
        }

        public bool Connected
        {
            get
            {
                if (Sock != null && Sock.Connected)
                    return true;
                else
                    return false;
            }
        }

        public Socket Sock
        {
            get { return m_Sock; }
            private set { m_Sock = value; }
        }

        public bool Ssl
        {
            get { return m_Ssl; }
            set { m_Ssl = value; }
        }

        public bool KeepAlive
        {
            set
            {
                if (value == true && KeepAliveTimer == null)
                {
                    //Sock.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
                    KeepAliveTimer = new Timer(10000);
                    KeepAliveTimer.Elapsed += KeepAliveTick;
                    KeepAliveTimer.Start();
                }
                else
                {
                    //Sock.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, false);
                    KeepAliveTimer.Stop();
                    KeepAliveTimer = null;
                }
            }
        }
        #endregion

        #region Events
        public event SocketEventHandler OnRead;
        public event SocketEventHandler OnWrite;
        public event EventHandler OnConnect;
        public event EventHandler OnDisconnect;
        public RemoteCertificateValidationCallback CertValidation;
        #endregion

        public BaseSocket()
        {

        }

        public BaseSocket(string server)
        {
            Server = server;
        }

        public BaseSocket(string server, int port)
            : this(server)
        {
            Port = port;
        }

        public BaseSocket(string server, int port, bool ssl)
            : this(server, port)
        {
            Ssl = ssl;
        }

        public virtual void Connect()
        {
            IPHostEntry host = Dns.GetHostEntry(Server); // Make some magic with server adress.
            foreach (IPAddress adress in host.AddressList)
            {
                Socket temp = new Socket(adress.AddressFamily, SocketType.Stream, ProtocolType.Tcp); // Create new, umm, temporary empty socket
                temp.Connect(new IPEndPoint(adress, Port)); // Try connect to specifed server 
                if (temp.Connected) // Connected? Yaaaaay!
                {
                    Sock = temp; // And set our socket to  
                    break; // Kill that loop, We don't need to connect to other ip.
                }
            }

            NetStream = new NetworkStream(Sock, false);
            Stream = NetStream;

            if (OnConnect != null)
                OnConnect(this, EventArgs.Empty);

            Read();
        }

        protected void StartSecurity()
        {
            lock (this)
            {
                SslStream = new SslStream(NetStream, false, new RemoteCertificateValidationCallback(onCertValidation), null);
                try
                {
                    SslStream.AuthenticateAsClient(this.Server, null, SslProtocols.Default, true);

                    /*DisplaySecurityLevel(SslStream);
                    DisplayCertificateInformation(SslStream);
                    DisplaySecurityServices(SslStream);
                    DisplayStreamProperties(SslStream);*/

                    Stream = SslStream;
                }
                catch (AuthenticationException ex)
                {
                    Console.WriteLine(ex.ToString());
                    Ssl = false;
                }
            }
        }

        public void Read()
        {
            Stream.BeginRead(Buffer, 0, BUFFER_SIZE, new AsyncCallback(EndRead), new object());
        }

        public void EndRead(IAsyncResult res)
        {
            try
            {
                int bytes = Stream.EndRead(res);

                if (bytes > 0)
                {
                    if (OnRead != null)
                        OnRead(this, Encoding.UTF8.GetString(Buffer, 0, bytes));
                    if (Connected)
                        Read(); // continue reading
                }
                else // Oh noes shit happens!
                {
                    Disconnect();
                }
            }
            catch (IOException ex) // shit heppens, again.
            {
                Disconnect();
            }
        }

        public void Write(string data)
        {
            Write(Encoding.UTF8.GetBytes(data));
        }

        public void Write(byte[] data)
        {
            lock (this)
            {
                if (!this.Connected)
                    return;

                if (OnWrite != null)
                    OnWrite(this, Encoding.UTF8.GetString(data));

                Console.WriteLine("Write: {0}", Encoding.UTF8.GetString(data));

                if (!Pending)
                {
                    Pending = true;
                    Stream.BeginWrite(data, 0, data.Length, new AsyncCallback(EndWrite), null);
                }
                else
                    WriteQueue.Enqueue(data);
            }
        }

        private void EndWrite(IAsyncResult res)
        {
            lock (this)
            {
                try
                {
                    Stream.EndWrite(res);
                }
                catch { } // HIHIHIHIHIHI

                if (WriteQueue.Count > 0)
                {
                    var data = WriteQueue.Dequeue();
                    Stream.BeginWrite(data, 0, data.Length, new AsyncCallback(EndWrite), null);
                }
                else
                    Pending = false;
            }
        }

        public virtual void Disconnect()
        {
            if (this.Connected)
            {
                if (OnDisconnect != null)
                    OnDisconnect(this, EventArgs.Empty);

                Sock.Shutdown(SocketShutdown.Both);
                Sock.Close();
            }
        }

        private void KeepAliveTick(object sender, ElapsedEventArgs e)
        {
            Write(" ");
        }

        public bool onCertValidation(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            if (CertValidation != null)
                return CertValidation(sender, certificate, chain, sslPolicyErrors);
            return true;
        }

        // Ssl debug

        static void DisplaySecurityLevel(SslStream stream)
        {
            Console.WriteLine("Cipher: {0} strength {1}", stream.CipherAlgorithm, stream.CipherStrength);
            Console.WriteLine("Hash: {0} strength {1}", stream.HashAlgorithm, stream.HashStrength);
            Console.WriteLine("Key exchange: {0} strength {1}", stream.KeyExchangeAlgorithm, stream.KeyExchangeStrength);
            Console.WriteLine("Protocol: {0}", stream.SslProtocol);
        }

        static void DisplaySecurityServices(SslStream stream)
        {
            Console.WriteLine("Is authenticated: {0} as server? {1}", stream.IsAuthenticated, stream.IsServer);
            Console.WriteLine("Is Signed: {0}", stream.IsSigned);
            Console.WriteLine("Is Encrypted: {0}", stream.IsEncrypted);
        }

        static void DisplayStreamProperties(SslStream stream)
        {
            Console.WriteLine("Can read: {0}, write {1}", stream.CanRead, stream.CanWrite);
            Console.WriteLine("Can timeout: {0}", stream.CanTimeout);
        }

        static void DisplayCertificateInformation(SslStream stream)
        {
            Console.WriteLine("Certificate revocation list checked: {0}", stream.CheckCertRevocationStatus);

            X509Certificate localCertificate = stream.LocalCertificate;
            if (stream.LocalCertificate != null)
            {
                Console.WriteLine("Local cert was issued to {0} and is valid from {1} until {2}.",
                    localCertificate.Subject,
                    localCertificate.GetEffectiveDateString(),
                    localCertificate.GetExpirationDateString());
            }
            else
            {
                Console.WriteLine("Local certificate is null.");
            }
            // Display the properties of the client's certificate.
            X509Certificate remoteCertificate = stream.RemoteCertificate;
            if (stream.RemoteCertificate != null)
            {
                Console.WriteLine("Remote cert was issued to {0} and is valid from {1} until {2}.",
                    remoteCertificate.Subject,
                    remoteCertificate.GetEffectiveDateString(),
                    remoteCertificate.GetExpirationDateString());
            }
            else
            {
                Console.WriteLine("Remote certificate is null.");
            }
        }
    }
}