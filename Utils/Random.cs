﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Utils
{
    class Random
    {
        private static System.Random generator = new System.Random();
 
        public static int GetRandom()
        {
            return generator.Next();
        }

        public static int GetRandom(int max)
        {
            return generator.Next(max);
        }

        public static int GetRandom(int min, int max)
        {
            return generator.Next(min, max);
        }
    }
}
