﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Protocol.IQ
{
    public class Query : XmlParser.Dom.Element
    {
        public Query(string uri = null) : base("query")
        {
            if (uri != null)
                this.SetAttribute("xmlns", uri);
        }

        public Query(XmlParser.Dom.Element element) : base(element) { }
    }
}
