﻿using System;

namespace DotXMPP.Protocol.IQ
{
    public enum IQType
    {
        Set,
        Get,
        Result,
        Error,
        Unknown = -1
    }
}