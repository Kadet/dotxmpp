﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Protocol.Message
{
    public class Message : AddressedStanza
    {
        public string Body
        {
            get { return Child("body").Content; }
            set { SetChild("body", value); }
        }

        public MessageType Type
        {
            get
            {
                if (GetAttribute("type") != null)
                    return (MessageType)Enum.Parse(typeof(MessageType), GetAttribute("type"), true);
                else
                    return MessageType.Chat;
            }
            set { SetAttribute("type", value.ToString().ToLower()); }
        }

        public Message() : base("message") { }
        public Message(string body, string to)
        {
            Body = body;
            To = to;
        }

        public Message(XmlParser.Dom.Element obj) : base(obj) { }
    }
}
