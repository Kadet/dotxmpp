﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Protocol.Sasl
{
    class Challenge : XmlParser.Dom.Element
    {
        public Challenge() : base("challenge", "", URI.Sasl) { }
    }
}
