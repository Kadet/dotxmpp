﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Protocol.Presence
{
    public class Presence : AddressedStanza
    {
        public SubscriptionType Subscripion
        {
            get
            {
                if (GetAttribute("subscription") != null)
                    return (SubscriptionType)Enum.Parse(typeof(SubscriptionType), GetAttribute("subscription"), true);
                else
                    return SubscriptionType.None;
            }
            set { SetAttribute("subscription", value.ToString().ToLower()); }
        }

        public ShowType Show
        {
            get
            {
                if (Child("show") != null)
                    return (ShowType)Enum.Parse(typeof(ShowType), Child("show").Content, true);
                else
                {
                    if (Type == PresenceType.Available)
                        return ShowType.Available;
                    else
                        return ShowType.Unavailable;
                }
            }
            set { SetChild("show", value.ToString().ToLower()); }
        }

        public PresenceType Type
        {
            get
            {
                if (GetAttribute("type") != null)
                    return (PresenceType)Enum.Parse(typeof(PresenceType), GetAttribute("type"), true);
                else
                    return PresenceType.Available;
            }
            set { SetAttribute("type", value.ToString().ToLower()); }
        }

        public string Status
        {
            get { return Child("status").Content; }
            set { SetChild("status", value); }
        }

        public int Priority
        {
            get { return Convert.ToInt32(Child("priority").Content); }
            set { SetChild("priority", value.ToString()); }
        }

        public Presence() : base("presence") { }
        public Presence(XmlParser.Dom.Element element) : base(element) { }

    }
}
