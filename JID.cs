﻿using System;
using System.Text.RegularExpressions;

namespace DotXMPP
{
    public class Jid : Protocol.Address
    {
        public string Login { get; set; }
        public string Resource { get; set; }

        public string Bare { get { return Login + "@" + Server; } }

        public bool IsTransport { get { return Login == null; } }

        public Jid(string jid)
        {

            if (Regex.IsMatch(jid, "[^@\\/\\\"\'\\s\\&\\:><]+@[a-z_\\-\\.]*[a-z]{2,3}(\\/{1}[a-zA-Z0-9\\-_\\.]*)?"))
            {
                Login = jid.Split('@')[0];
                Server = jid.Split('@')[1].Split('/')[0];

                if (jid.Contains("/"))
                    Resource = jid.Split('@')[1].Split('/')[1];
            }
            else if (Regex.IsMatch(jid, "[a-z_\\-\\.]*[a-z]{2,3}"))
                Server = jid;
            else
                throw new Exceptions.XMPPException("JID is invalid", Exceptions.XMPPException.Type.JIDInvalid);
        }

        public Jid(string login, string server, string resource = null)
        {
            Login = login;
            Server = server;
            Resource = resource;
        }

        public override string ToString()
        {
            if (!IsTransport)
                return Login + "@" + Server + (Resource != null ? "/" + Resource : "");
            else
                return Server;
        }

        public static Protocol.Address FromString(string str)
        {
            if(Regex.IsMatch(str, "[^@\\/\\\"\'\\s\\&\\:><]+@[a-z_\\-\\.]*[a-z]{2,3}(\\/{1}[a-zA-Z0-9\\-_\\.]*)?")
                return new Jid(str);
            else
                return new Protocol.Address(str);
        }
    }
}
