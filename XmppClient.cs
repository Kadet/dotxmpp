﻿using System;

using DotXMPP.Protocol.Message;
using DotXMPP.Protocol.IQ;
using DotXMPP.Protocol.Presence;
using DotXMPP.Protocol.Stream;
using DotXMPP.Protocol.Sasl;


namespace DotXMPP
{
    public delegate void MessageHandler(object sender, Message message);
    public delegate void PresenceHandler(object sender, Presence presence);
    public delegate void IqHandler(object sender, Iq iq);

    public class XmppClient : XmppConnection
    {
        public Protocol.Roster.Roster Roster { get; private set; }

        public string Resource { get; set; }

        public string ClientLanguage { get; set; }
        public string ServerLanguage { get; set; }

        public bool AutoRoster { get; set; }

        public event EventHandler OnLogged;

        public event MessageHandler OnMessage;
        public event PresenceHandler OnPresence;
        public event IqHandler OnIq;

        public event EventHandler OnAuth;
        public event EventHandler OnAuthError;

        public event EventHandler OnBind;
        public event EventHandler OnBindError;

        public event EventHandler OnSessionStart;
        public event EventHandler OnSessionError;


        public bool Secured { get; private set; }
        public bool Authorized { get; private set; }
        public bool Binded { get; private set; }

        private SaslHandler m_SaslHandler;

        public XmppClient(Jid jid, string password, int port = 5222)
            : base(jid.Server, port)
        {
            Username = jid.Login;
            Resource = jid.Resource;
            Password = password;

            AutoRoster = true;

            Roster = new Protocol.Roster.Roster(this);

            OnElement += XmppClient_OnElement;
            OnConnect += XmppClient_OnConnect;
        }

        void XmppClient_OnConnect(object sender, EventArgs e)
        {
            StartStream(ClientLanguage); // hardcoded :)
            State = ConnectionState.Connected;
            Secured = false;
            m_SaslHandler = new SaslHandler(this);
        }

        void XmppClient_OnElement(object sender, XmlParser.Dom.Element element)
        {
            if (element is Iq)
            {
                if (OnIq != null)
                    OnIq(this, element as Iq);
            }

            if (element is Presence)
            {
                if (OnPresence != null)
                    OnPresence(this, element as Presence);
            }

            if (element is Message)
            {
                if (OnMessage != null)
                    OnMessage(this, element as Message);
            }

            if (element is Features)
            {
                var e = element as Features;

                if (e.SupportTLS && !Secured)
                {
                    State = ConnectionState.Securing;
                    Write(new Protocol.Tls.StartTls());
                }

                if (Authorized && e.Bind)
                {
                    Bind();
                }
            }
            if (element is Protocol.Tls.Proceed)
            {
                StartSecurity();
                StartStream(ClientLanguage);
                State = ConnectionState.secured;
                Secured = true;
            }
        }

        public void SendPresence(Presence presence)
        {
            Write(presence);
        }

        public void SendMessage(Message message)
        {
            Write(message);
        }

        public void Authorize()
        {
            Authorized = true;
            m_SaslHandler.TurnOff();

            if(OnAuth != null)
                OnAuth(this, EventArgs.Empty);
        }

        public void AuthorizationError()
        {
            if (OnAuthError != null)
                OnAuthError(this, EventArgs.Empty);
            Disconnect();
        }

        public void Bind()
        {
            var bind = new Protocol.Bind.BindIq(new Protocol.Bind.Bind(Resource));
            bind.Type = IQType.Set;
            State = ConnectionState.Binding;
            Write(bind);
            BeginWaitingForId(bind.Id, new AsyncCallback(BindResult));
        }

        private void BindResult(IAsyncResult ar)
        {
            var result = EndWaiting(ar) as Iq;
           
            if (result.Type == IQType.Result)
            {
                this.State = ConnectionState.Binded;
                if (OnBind != null)
                    OnBind(this, EventArgs.Empty);

                StartSession();
            }
            else
            {
                if (OnBindError != null)
                    OnBindError(this, EventArgs.Empty);
                Disconnect();
            }
        }

        private void StartSession()
        {
            var sess = new Protocol.Bind.Session();
            State = ConnectionState.SessionStarting;
            Write(sess);
            BeginWaitingForId(sess.Id, new AsyncCallback(SessionResult));
        }

        private void SessionResult(IAsyncResult ar)
        {
            var element = EndWaiting(ar) as Iq;

            if (element.Type == IQType.Result)
            {
                State = ConnectionState.Running;

                if (OnSessionStart != null)
                    OnSessionStart(this, EventArgs.Empty);

                if (AutoRoster)
                    Roster.Get();
            }
            else
            {
                if (OnSessionError != null)
                    OnSessionError(this, EventArgs.Empty);
                Disconnect();
            }
        }
    }
}
