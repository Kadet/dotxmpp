﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DotXMPP.Net.Sasl;

namespace DotXMPP.Protocol.Sasl
{
    class SaslHandler
    {
        private XmppClient m_Client;
        private Net.Sasl.Mechanism m_Mechanism;

        public SaslHandler(XmppClient client)
        {
            m_Client = client;
            m_Client.OnElement += client_OnElement;
        }

        void client_OnElement(object sender, XmlParser.Dom.Element element)
        {
            if (element is Stream.Features)
            {
                var e = element as Stream.Features;

                if (m_Client.Secured || !e.SupportTLS)
                {
                    /*if (e.Mechanisms.Support("DIGEST-MD5"))
                        m_Mechanism = new DigestMD5(m_Client);
                    else */if (e.Mechanisms.Support("PLAIN"))
                        m_Mechanism = new Plain(m_Client);

                    if (m_Mechanism != null)
                    {
                        m_Mechanism.Server = m_Client.Server;
                        m_Mechanism.Username = m_Client.Username;
                        m_Mechanism.Password = m_Client.Password;

                        m_Client.State = ConnectionState.Authorising;

                        m_Mechanism.Init();
                    }
                }
            }

            if (element is Challenge)
            {
                if (m_Mechanism != null)
                    m_Mechanism.Parse(element);
            }

            if (element is Success)
            {
                m_Client.StartStream(m_Client.ClientLanguage);
                m_Client.State = ConnectionState.Authorised;
                m_Client.Authorize();
            }

            if (element is Failure)
            {
                m_Client.State = ConnectionState.Unauthorised;
                m_Client.AuthorizationError();
            }
        }

        public void TurnOff()
        {
            m_Mechanism = null;
            m_Client.OnElement -= client_OnElement;
        }
    }
}
