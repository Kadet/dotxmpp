﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DotXMPP.Protocol.Roster
{
    public class RosterItem : XmlParser.Dom.Element
    {
        public RosterItem()
            : base("item")
        {
        }

        public RosterItem(Jid jid, string name)
            : base("item")
        {
            Jid = jid;
            Name = name;
        }

        public RosterItem(Jid jid, string name, string[] groups)
            : this(jid, name)
        {
            foreach (var group in groups)
                this.AddChild("group", group);
        }

        public Jid Jid
        {
            get { return new Jid(this.GetAttribute("jid"));  }
            set
            {
                if (value != null)
                    this.SetAttribute("jid", value.ToString());
                else
                    this.RemoveAttribute("jid");
            }
        }

        public string Name
        {
            get { return (this.GetAttribute("name") != null ? this.GetAttribute("name") : this.GetAttribute("jid")); }
            set
            {
                if (value != null)
                    this.SetAttribute("name", value.ToString());
                else
                    this.RemoveAttribute("name");
            }
        }

        public List<string> Groups
        {
            get
            {
                List<string> list = new List<string>();
                var nodes = this.GetChilds("group");
                if(nodes != null)
                    foreach(var group in nodes)
                        list.Add(group.Content);
                return list;
            }

            set
            {
                this.Childs.Clear();
                foreach (var group in value)
                    this.AddChild("group", group);
            }
        }

        public Presence.AskType Ask
        {
            get
            {
                if (GetAttribute("ask") != null)
                    return (Presence.AskType)Enum.Parse(typeof(Presence.AskType), GetAttribute("ask"), true);
                else
                    return Presence.AskType.None;
            }
            set { SetAttribute("ask", value.ToString().ToLower()); }
        }

        public Presence.SubscriptionType Subscripion
        {
            get 
            {
                if (GetAttribute("subscription") != null)
                    return (Presence.SubscriptionType)Enum.Parse(typeof(Presence.SubscriptionType), GetAttribute("subscription"), true);
                else
                    return Presence.SubscriptionType.None;
            }
            set { SetAttribute("subscription", value.ToString().ToLower()); }
        }

        public RosterItem(XmlParser.Dom.Element element) : base(element) { }
    }
}
