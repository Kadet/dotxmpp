﻿namespace DotXMPP.Protocol.Presence
{
    public enum PresenceType
    {
        Unavailable,
        Probe,
        Available
    }
}