﻿using DotXMPP.Exceptions;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using XmlParser.Dom;

namespace DotXMPP
{
    public delegate void ElementHandler(object sender, Element element);

    public class XMPPSocket : Net.BaseSocket
    {
        public event ElementHandler OnElement;
        public event SocketEventHandler OnWriteXml;
        public event SocketEventHandler OnReadXml;

        public event EventHandler OnStreamEnd;
        public event ElementHandler OnStreamStart;

        protected List<Element> Packets = new List<Element>();

        private StringBuilder PacketBuffer = new StringBuilder();

        public XMPPSocket(string server, int port = 5222, bool ssl = true) : base(server, port, ssl)
        {
            this.OnDisconnect += new EventHandler(onDisconnect);
            this.OnRead += new SocketEventHandler(onRead);
            this.OnWrite += new SocketEventHandler(onWrite);
        }

        void onWrite(object sender, string content)
        {
            if (OnWriteXml != null)
                OnWriteXml(this, content);
        }

        public void onDisconnect(object sender, EventArgs args)
        {
            this.Write("</stream:stream>");
        }

        public void Write(Element xml) // Makes life easier
        {
            Write(xml.Xml);
        }

        protected void onRead(object sender, string content)
        {
            Console.WriteLine("Read: {0}", content);
            if (!this.Connected)
                throw new XMPPSocketException("Not connected to server!", XMPPSocketException.Type.NotConnected); // Oh noes!

            PacketBuffer.Append(content);
            var ret = PacketBuffer.ToString();
            if (ret.Length > 3 && Regex.IsMatch(ret.Substring(ret.Length - 3), @"('/|" + "\"" + @"/|iq|ge|ce|am|es|se|ss|ge|re)>$"))
                GetPackets();
        }

        public void GetPackets()
        {
            string xml = PacketBuffer.ToString(); // Get data from server.
            if (xml == "" || !(xml.Length > 3 && Regex.IsMatch(xml.Substring(xml.Length - 3), @"('/|" + "\"" + @"/|iq|ge|ce|am|es|se|ss|ge|re)>$"))) return;

            if (OnReadXml != null)
                OnReadXml(this, xml);

            Regex split = new Regex(@"<(/stream:stream|stream|iq|presence|message|proceed|failure|challange|response|success)"); // Regex for splitting packets
            var packets = split.Split(xml); // split data from server to individual packetets

            for (int i = 1; i < packets.Length; i += 2) // go through packets
            {
                if (!packets[1].Contains("stream:stream") && !packets[1].Contains("xml"))
                {
                    AddPacket("<" + packets[i] + packets[i + 1]); // Add current to queue, agh, so difficult word
                }
                else if (packets[1].Contains("stream:stream") && !packets[1].Contains("/stream:stream"))
                {
                    if(OnStreamStart != null)
                        OnStreamStart(this, XmlParser.Parser.Parse("<" + packets[i] + packets[i + 1] + "</stream:stream>"));
                }
            }


            if (xml.Contains("</stream:stream>"))
            {
                if(OnStreamEnd != null)
                    OnStreamEnd(this, EventArgs.Empty);
                Disconnect();
            }

            PacketBuffer.Clear();
        }

        public Element NextPacket()
        {
            if (Packets.Count > 0) // Are packets in queue?
            {
                var packet = Packets.GetRange(0, 1)[0];
                Packets.RemoveRange(0, 1);
                return packet; // Yep? Get next.
            }
            else
                return null; // Nope? Return, null, zero, nothing!
        }

        protected void AddPacket(string xml)
        {
            if (!xml.Contains("stream:stream") && !xml.Contains("<?xml")) // Initial packet?
            {
#if DEBUG
//                Console.WriteLine(xml); // Debuging, debuging, debuging - Programmer
#endif
                var element = Protocol.StanzaFactory.Get(XmlParser.Parser.Parse(xml));
                if (this.OnElement != null)
                    OnElement(this, element);
                Packets.Add(element); // No? Add it to queue.
            }
        }
    }
}
