﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Protocol.Stream
{
    class Features : Stanza
    {
        public bool SupportTLS
        {
            get { return HasChild("starttls"); }
        }

        public Sasl.Mechanisms Mechanisms
        {
            get { return Child("mechanisms") as Sasl.Mechanisms; }
        }

        public bool Bind
        {
            get { return HasChild("bind"); }
        }
    }
}
