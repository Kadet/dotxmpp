﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Protocol.Presence
{
    public enum SubscriptionType
    {
        None,
        Both,
        From,
        To,
        Remove
    }
}
