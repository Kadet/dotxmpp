﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Protocol.Bind
{
    class Session : IQ.Iq
    {
        public Session()
            : base()
        {
            AddChild(new XmlParser.Dom.Element("session", "", URI.Session));
            Type = IQ.IQType.Set;
        }
    }
}
