﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Protocol.Tls
{
    public class StartTls : Stanza
    {
        public StartTls()
            : base("starttls")
        {
            NameSpace = URI.Tls;
        }

        bool Required
        {
            get { return HasChild("required"); }
            set
            {
                if (value == false)
                    RemoveChild("required");
                else
                    SetChild("required", "");
            }
        }
    }
}
