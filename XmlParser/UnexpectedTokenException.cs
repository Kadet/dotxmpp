﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XmlParser
{
    public class UnexpectedTokenException : Exception
    {
        public char Token { get; private set; }
        public int Pos { get; private set; }

        public UnexpectedTokenException(char token, int pos)
        {
            Token = token;
            Pos = pos;
        }
    }
}
