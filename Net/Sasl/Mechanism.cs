﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XmlParser.Dom;

namespace DotXMPP.Net.Sasl
{
    abstract class Mechanism
    {
        #region << Properties >>
        public string Server   { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        #endregion
        public XmppConnection m_Connection = null;

        public Mechanism(XmppConnection conn)
        {
            m_Connection = conn;
        }

        public abstract void Parse(Element element);
        public abstract void Init();
    }
}
