﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XmlParser
{
    class XmlParserException : Exception
    {
        public XmlParserException() : base() { }
        public XmlParserException(string message) : base(message) { }
    }
}
