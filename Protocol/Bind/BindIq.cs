﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Protocol.Bind
{
    class BindIq : IQ.Iq
    {
        public Bind Bind 
        {
            get { return Child("bind") as Bind; }
            set { SetChild("bind", value); }
        }

        public BindIq() : base() { }
        public BindIq(Bind bind = null)
            : base()
        {
            Bind = bind;
        }
    }
}
