﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XmlParser
{
    class WrongNameException : Exception
    {
        public WrongNameException(string message) : base(message) { }
    }
}
