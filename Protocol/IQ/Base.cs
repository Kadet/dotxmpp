﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace DotXMPP.Protocol.IQ
{
    /*abstract */ public class Iq : Protocol.AddressedStanza
    {
        public IQType Type 
        {
            get
            {
                if (this.GetAttribute("type") != null)
                {
                    switch (this.GetAttribute("type").ToString().ToLower())
                    {
                        case "set":
                            return IQType.Set;
                        case "get":
                            return IQType.Get;
                        case "result":
                            return IQType.Result;
                        case "error":
                            return IQType.Error;
                        default:
                            return IQType.Unknown;
                    }
                }
                else return IQType.Unknown;
            }

            set
            {
                if (value != IQType.Unknown)
                    this.SetAttribute("type", value.ToString().ToLower());
                else
                    this.SetAttribute("type", null);
            }
        }

        public IQ.Query Query
        {
            get
            {
                var test = this.Child("query");
                if (this.Child("query") != null)
                    return new IQ.Query(this.Child("query"));
                else
                    return null;
            }

            set { this.SetChild("query", value); }
        }

        #region constructors
        public Iq()
            : base("iq")
        {

        }

        public Iq(XmlParser.Dom.Element obj) : base(obj) { }
        #endregion
    }
}
