﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Protocol.Tls
{
    public class Proceed : Stanza
    {
        public Proceed()
            : base("proceed")
        {
            NameSpace = URI.Tls;
        }
    }
}
