﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Protocol.Sasl
{
    public class Mechanisms : XmlParser.Dom.Element
    {
        public bool Support(string mechanism)
        {
            var list = GetChilds("mechanism");
            foreach (var mech in list)
            {
                if (mech.Content == mechanism.ToUpper())
                    return true;
            }
            return false;
        }
    }
}
