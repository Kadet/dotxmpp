﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Text;

namespace DotXMPP.Protocol.Roster
{
    public class RosterQuery : IQ.Query
    {
        public List<RosterItem> Items
        {
            get
            {
                List<RosterItem> list = new List<RosterItem>();
                var nodes = this.GetChilds("item");
                if (nodes == null)
                    return list;
                foreach (XmlParser.Dom.Element item in nodes)
                    list.Add(new RosterItem(item));
                return list;
            }

            /*private set
            {
                Items = value;
            }*/
        }

        public RosterQuery() : base(URI.Roster) { }
        public RosterQuery(XmlParser.Dom.Element element) : base(element) { }

        public void AddItem(RosterItem item)
        {
            Items.Add(item);
            this.AddChild(item);
        }

        public void RemoveItem(RosterItem item)
        {
            var nodes = this.GetChilds("item");

            foreach (var buf in nodes)
            {
                if (buf.Xml == item.Xml)
                {
                    this.RemoveChild("item", nodes.IndexOf(buf));
                    return;
                }
            }
            throw new ArgumentException("Given item doesn't exist in this query!", "item");
        }

        public void EditItem(Jid jid, RosterItem item)
        {
            var nodes = this.GetChilds("item");
            RosterItem buf = null;
            foreach (XmlParser.Dom.Element child in nodes)
            {
                buf = new RosterItem(child);
                if (buf.Jid.Bare == jid.Bare)
                {
                    this.SetChild("item", item, nodes.IndexOf(child));
                    return;
                }
            }

            throw new ArgumentException("Given item doesn't exist in this query!", "item");
        }
    }
}
