﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP
{
    class URI
    {
        public const string StreamClient = "jabber:client";
        public const string Stream = "http://etherx.jabber.org/streams";

        public const string Roster = "jabber:iq:roster";

        public const string Auth = "jabber:iq:auth";

        public const string Tls = "urn:ietf:params:xml:ns:xmpp-tls";
        public const string Sasl = "urn:ietf:params:xml:ns:xmpp-sasl";
        public const string Bind = "urn:ietf:params:xml:ns:xmpp-bind";
        public const string Session = "urn:ietf:params:xml:ns:xmpp-session";
    }
}
