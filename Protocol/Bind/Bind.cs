﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Protocol.Bind
{
    class Bind : XmlParser.Dom.Element
    {
        public string Resource
        {
            get 
            {
                var ret = Child("resource");
                if (ret != null)
                    return ret.Content;
                else
                    return null;
            }

            set
            {
                if (value != String.Empty)
                    SetChild("resource", value);
                else
                    RemoveChild("resource");
            }
        }
        public Bind() : base("bind", "", URI.Bind) { }
        public Bind(string resource = "")
            : this()
        {
            this.Resource = resource;
        }
    }
}
