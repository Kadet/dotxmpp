﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XmlParser.Dom;

namespace DotXMPP
{
    public delegate void ConnectionStateChanged(object sender, ConnectionState state);
    public class XmppConnection : XMPPSocket
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public string StreamId { get; private set; }

        public virtual Jid Jid
        {
            get{ return new Jid(Username + "@" + Server); }
        }

        private ConnectionState m_State = ConnectionState.Disconnected;
        public virtual ConnectionState State 
        { 
            get { return m_State; }
            set
            {
                m_State = value;
                if (OnStateChanged != null)
                    OnStateChanged(this, value);
            }
        }

        public event ConnectionStateChanged OnStateChanged;

        public XmppConnection(string server, int port = 5222) : base(server, port) 
        { 
            OnStreamStart += onStreamStart;
        }

        private void onStreamStart(object sender, Element element)
        {
            StreamId = element.GetAttribute("id");
        }

        #region << waiting for packets >>
        public Element GetPacketByID(string id)
        {
            foreach (var element in this.Packets)
            {
                if (element.GetAttribute("id") == id)
                    return element;
            }
            return null;
        }

        public Element GetPacketByTag(string tag)
        {
            foreach (var element in this.Packets)
            {
                if (element.Tag == tag)
                    return element;
            }
            return null;
        }

        public void BeginWaitingForTag(string tag, AsyncCallback callback)
        {
            WaitDelegate del = new WaitDelegate(WaitForTag);
            del.BeginInvoke(tag, callback, del);
        }

        public void BeginWaitingForId(string id, AsyncCallback callback)
        {
            WaitDelegate del = new WaitDelegate(WaitForId);
            del.BeginInvoke(id, callback, del);
        }

        public Element EndWaiting(IAsyncResult res)
        {
            var del = (WaitDelegate)res.AsyncState;
            return del.EndInvoke(res);
        }

        private Element WaitForId(string id)
        {
            DateTime start = DateTime.Now;
            Element ret = null;
            while (DateTime.Now.Subtract(start).TotalSeconds < 60)
            {
                this.GetPackets();
                ret = this.GetPacketByID(id);

                if (ret != null)
                    return ret;

                System.Threading.Thread.Sleep(50);
            }
            return null;
        }

        private Element WaitForTag(string tag)
        {
            DateTime start = DateTime.Now;
            Element ret = null;
            while (DateTime.Now.Subtract(start).TotalSeconds < 60)
            {
                this.GetPackets();
                ret = this.GetPacketByTag(tag);

                if (ret != null)
                    return ret;

                System.Threading.Thread.Sleep(50);
            }
            return null;
        }
        #endregion

        public void StartStream(string language)
        {
            this.Write("<?xml version='1.0'?>");
            var stream = "<stream:stream" +
            " xmlns=\"" + URI.StreamClient + "\"" +
            " version=\"1.0\"" +
            " to=\"" + Server + "\"" +
            " xml:lang=\"" + language + "\"" +
            " xmlns:xml=\"http://www.w3.org/XML/1998/namespace\"" +
            " xmlns:stream=\"" + URI.Stream + "\">";

            this.Write(stream);
        }
        override public void Connect()
        {
            base.Connect();
            State = ConnectionState.Connecting;
        }

        override public void Disconnect()
        {
            base.Disconnect();
            State = ConnectionState.Disconnected;
        }

        delegate Element WaitDelegate(string wat);
    }
}
