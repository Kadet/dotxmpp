﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Exceptions
{
    public class XMPPException : Exception
    {
        public enum Type
        {
            AuthenticationFailed,
            JIDInvalid
        }

        public XMPPException()
            : base ()
        { }

        public XMPPException(string message, XMPPException.Type Code)
            : base(message)
        {
            this.p_Code = Code;
        }

        protected XMPPException.Type p_Code;
        public XMPPException.Type Code { get { return this.p_Code; } }
    }
}
