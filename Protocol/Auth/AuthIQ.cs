﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DotXMPP.Protocol.Auth
{
    public class AuthIQ : IQ.Iq
    {
        enum AuthType
        {
            Plain,
            Digest
        }

        public AuthIQ()
            : base()
        {
            this.Id = "auth_" + Utils.Random.GetRandom(0, 100000).ToString();
        }

        public AuthIQ(string username, string password, string resource = null)
            : this()
        {
            this.Query = new Plain(username, password, resource);
        }

        public AuthIQ(XmlParser.Dom.Element obj) : base(obj) { }
    }
}
