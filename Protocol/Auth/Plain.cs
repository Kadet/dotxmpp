﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Protocol.Auth
{
    public class Plain : IQ.Query
    {
        public string Username
        {
            get { return this.Child("username").Content; }
            set { this.SetChild("username", value); }
        }

        public string Password
        {
            get { return this.Child("password").Content; }
            set { this.SetChild("password", value); }
        }

        public string Resource
        {
            get { return this.Child("resource").Content; }
            set { this.SetChild("resource", value); }
        }

        public Plain() : base(URI.Auth)
        {

        }

        public Plain(string login, string password, string resource = null)
            : base(URI.Auth)
        {
            Username = login;
            Password = password;
            Resource = resource;
        }
    }
}
