﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Exceptions
{
    public class XMPPSocketException : Exception
    {
        public enum Type
        {
            NotConnected,
            CantConnect
        }

        public XMPPSocketException()
            : base()
        { }

        public XMPPSocketException(string message, XMPPSocketException.Type Code)
            : base(message)
        {
            this.p_Code = Code;
        }

        protected XMPPSocketException.Type p_Code;
        public XMPPSocketException.Type Code { get { return this.p_Code; } }
    }
}
