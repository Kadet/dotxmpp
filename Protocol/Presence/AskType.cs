﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Protocol.Presence
{
    public enum AskType
    {
        Subscribe,
        Unsubscribe,
        None = -1
    }
}
