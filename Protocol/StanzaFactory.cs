﻿using System;
using System.Collections;

using XmlParser.Dom;

namespace DotXMPP.Protocol
{
    static class StanzaFactory
    {
        private static Hashtable Types = new Hashtable();

        static StanzaFactory()
        {
            // Basic packets
            Add("iq"        , null          , typeof(IQ.Iq)             );
            Add("presence"  , null          , typeof(Presence.Presence) );
            Add("message"   , null          , typeof(Message.Message)   );

            Add("stream:features", null     , typeof(Stream.Features)   );
            Add("bind"      , URI.Bind      , typeof(Bind.Bind)         );

            // SASL
            Add("mechanisms", URI.Sasl      , typeof(Sasl.Mechanisms)   );
            Add("challenge" , URI.Sasl      , typeof(Sasl.Challenge)    );
            Add("failure"   , URI.Sasl      , typeof(Sasl.Failure)      );
            Add("success"   , URI.Sasl      , typeof(Sasl.Success)      );

            // Roster
            Add("query"     , URI.Roster    , typeof(Roster.RosterQuery));
            Add("item"      , URI.Roster    , typeof(Roster.RosterItem) );

            // Tls
            Add("starttls"  , URI.Tls       , typeof(Tls.StartTls)      );
            Add("proceed"   , URI.Tls       , typeof(Tls.Proceed)       );
        }

        public static Element Get(Element element)
        {
            if (Types.Contains(element.Tag + "&" + element.NameSpace))
            {
                System.Type t = Types[element.Tag + "&" + element.NameSpace] as System.Type;
                var ret = System.Activator.CreateInstance(t) as Element;
                ret.FromElement(element);

                for(int i = 0; i < ret.Childs.Count; i++)
                    ret.Childs[i] = Get(ret.Childs[i]);

                return ret;
            }
            else return element;
        }

        private static void Add(string tag, string ns, System.Type type)
        {
            Types.Add(tag + "&" + ns, type);
        }
    }
}
