﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Protocol
{
    public class AddressedStanza : Stanza
    {
        public Address From
        {
            get { return Jid.FromString(GetAttribute("from")); }
            set { SetAttribute("from", value.ToString()); }
        }

        public Address To
        {
            get { return Jid.FromString(GetAttribute("to")); }
            set { SetAttribute("to", value.ToString()); }
        }

        public AddressedStanza(XmlParser.Dom.Element obj) : base(obj) { }

        public AddressedStanza() : base() { }
        public AddressedStanza(string tag) : base(tag) { }
        public AddressedStanza(string tag, string id) : base(tag, id) { }

        public AddressedStanza(string tag, string id, Address from, Address to)
            : base(tag, id)
        {
            From = from;
            To = to;
        }
    }
}
