﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Protocol.Message
{
    public enum MessageType
    {
        None,
        Chat,
        Headline,
        Groupchat
    }
}
