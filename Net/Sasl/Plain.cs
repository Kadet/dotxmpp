﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Net.Sasl
{
    class Plain : Mechanism
    {
        public Plain(XmppConnection conn) : base(conn) { }

        private string Encoded()
        {
            // NULL + Username + NULL + Password
            var message = '\0' + Username + '\0' + Password;
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(message));
        }

        override public void  Init()
        {
            m_Connection.Write(new Protocol.Sasl.Auth("PLAIN", Encoded()));
        }

        override public void Parse(XmlParser.Dom.Element element)
        {
 	        // Nothing here for plain :)
        }
    }
}
