﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Protocol.Presence
{
    public enum ShowType
    {
        Unavailable,
        Dnd,
        Xa,
        Away,
        Available,
        Chat
    }
}
