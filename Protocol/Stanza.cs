﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XmlParser.Dom;

namespace DotXMPP.Protocol
{
    public class Stanza : Element
    {
        public string Id
        {
            get { return this.GetAttribute("id"); }
            set { this.SetAttribute("id", value); }
        }

        public Stanza() : base() 
        {
            Id = "dot_" + String.Format("{0:X}", Utils.Random.GetRandom(1500000));
        }

        public Stanza(string tag) : base(tag) 
        { 
            Id = "dot_" + String.Format("{0:X}", Utils.Random.GetRandom(1500000)); 
        }

        public Stanza(string tag, string id)
            : base(tag) 
        {
            Id = id;
        }

        public Stanza(Element obj) : base(obj) { }
    }
}
