﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace XmlParser
{
    public static class Parser
    {
        private const char T_LT = '<';
        private const char T_GT = '>';
        private const char T_SPACE = ' ';
        private const char T_SLASH = '/';
        private const char T_QMARK = '?';
        private const char T_QUOTE = '"';
        private const char T_SQUOT = '\'';
        private const char T_EXCLA = '!';
        private const char T_DASH = '-';
        private const char T_EQUAL = '=';

        public static Dom.Element Parse(string xml)
        {
            Dom.Element root = null;

            for (int offset = 0; offset < xml.Length; offset++)
            {
                char c = xml[offset], cn = '\0', c2n = '\0';

                if (offset + 1 < xml.Length)
                    cn = xml[offset + 1];
                if (offset + 2 < xml.Length)
                    c2n = xml[offset + 2];

                if (c == T_LT)
                {
                    if (cn == T_QMARK)
                        ParseMetaTag(xml, ref offset);
                    else if (cn == T_EXCLA)
                    {
                        if (offset + 9 < xml.Length && xml.Substring(offset, 9) == "<![CDATA[")
                            ParseCDATA(xml, ref offset);
                        else if (offset + 4 < xml.Length && xml.Substring(offset, 4) == "<!--")
                            ParseComment(xml, ref offset);
                        else
                            ParseExclamination(xml, ref offset);
                    }
                    else
                        root = ParseElement(xml, ref offset);
                    continue;
                }
            }
            return root;
        }

        private static string ParseCDATA(string xml, ref int offset)
        {
            offset += 9;
            StringBuilder ret = new StringBuilder();
            for (; offset < xml.Length; offset++)
            {
                char c = xml[offset], cn = '\0', c2n = '\0';

                if (offset + 1 < xml.Length)
                    cn = xml[offset + 1];
                if (offset + 2 < xml.Length)
                    c2n = xml[offset + 2];

                if (c == ']' && cn == ']' && c2n == '>')
                {
                    offset += 3;
                    return ret.ToString();
                }
                else
                    ret.Append(c);
            }
            throw new XmlParserException("No ending tag for CDATA section");
        }

        private static void ParseComment(string xml, ref int offset)
        {
            offset += 4;
            for (; offset < xml.Length; offset++)
            {
                char c = xml[offset], cn = '\0', c2n = '\0';

                if (offset + 1 < xml.Length)
                    cn = xml[offset + 1];
                if (offset + 2 < xml.Length)
                    c2n = xml[offset + 2];

                if (c == T_DASH && cn == T_DASH && c2n == T_GT)
                {
                    offset++;
                    return;
                }
            }
            throw new XmlParserException("No ending tag for comment");
        }

        private static void ParseExclamination(string xml, ref int offset)
        {
            offset += 2;
            for (; offset < xml.Length; offset++)
            {
                char c = xml[offset], cn = '\0', c2n = '\0';

                if (offset + 1 < xml.Length)
                    cn = xml[offset + 1];
                if (offset + 2 < xml.Length)
                    c2n = xml[offset + 2];

                if (c == T_EXCLA && cn == T_GT)
                {
                    offset++;
                    return;
                }
            }
            throw new XmlParserException("No ending tag for exclamination");
        }

        private static void ParseMetaTag(string xml, ref int offset)
        {
            offset += 2;
            for (; offset < xml.Length; offset++)
            {
                char c = xml[offset], cn = '\0', c2n = '\0';

                if (offset + 1 < xml.Length)
                    cn = xml[offset + 1];
                if (offset + 2 < xml.Length)
                    c2n = xml[offset + 2];

                if (c == T_QMARK && cn == T_GT)
                {
                    offset++;
                    return;
                }
            }
            throw new XmlParserException("No ending tag for meta tag");
        }

        private static Dom.Element ParseElement(string xml, ref int offset, Dom.Element parent = null)
        {
            Dom.Element ret = new Dom.Element();
            ret.Parent = parent;

            bool inNodeName = true, inAttrName = false, inAttrValue = false, inContent = false;
            StringBuilder tag = new StringBuilder(), attrName = new StringBuilder(), attrValue = new StringBuilder(), content = new StringBuilder();
            char attrStart = '\0';

            offset++;
            for (; offset < xml.Length; offset++)
            {
                char c = xml[offset], cn = '\0', c2n = '\0';

                if (offset + 1 < xml.Length)
                    cn = xml[offset + 1];
                if (offset + 2 < xml.Length)
                    c2n = xml[offset + 2];

                if (!inContent)
                {
                    if (inNodeName)
                    {
                        if (c == T_SPACE || c == T_GT || (c == T_SLASH && cn == T_GT))
                        {
                            ret.Tag = tag.ToString();
                            if (!Regex.IsMatch(tag.ToString(), "[a-zA-Z_\\-\\.\\:]+"))
                                throw new WrongNameException("XML tag name contains not allowed chars");
                            if (c == T_GT)
                                inContent = true;

                            if (c == T_SLASH && cn == T_GT)
                                return ret;

                            inNodeName = false;
                            continue;
                        }

                        tag.Append(c);
                    }

                    if (c == T_GT)
                    {
                        if (inAttrName || inAttrValue)
                            throw new UnexpectedTokenException(T_GT, offset);

                        inContent = true;
                        continue;
                    }

                    if (c == T_SLASH && !inAttrValue)
                    {
                        if (cn == T_GT)
                            return ret;
                        else
                            throw new UnexpectedTokenException(T_SLASH, offset);
                    }

                    if (!inNodeName && !inAttrName && !inAttrValue && Regex.IsMatch(c.ToString(), "[a-zA-Z_\\-]"))
                        inAttrName = true;

                    if (inAttrName)
                    {
                        if (c == T_EQUAL)
                        {
                            if (cn == T_QUOTE || cn == T_SQUOT)
                            {
                                offset++;
                                inAttrName = false;
                                inAttrValue = true;
                                attrStart = cn;
                                continue;
                            }
                            else
                                throw new UnexpectedTokenException(cn, offset + 1);
                        }
                        attrName.Append(c);
                    }

                    if (inAttrValue)
                    {
                        if (c == attrStart)
                        {
                            inAttrValue = false;
                            ret.SetAttribute(attrName.ToString(), attrValue.ToString());
                            attrValue.Clear();
                            attrName.Clear();
                            continue;
                        }
                        attrValue.Append(c);
                    }

                    if (c == T_SPACE)
                        continue;
                }
                else
                {
                    if (c == T_LT)
                    {
                        if (cn == T_QMARK)
                            ParseMetaTag(xml, ref offset);
                        else if (cn == T_EXCLA)
                        {
                            if (offset + 9 < xml.Length && xml.Substring(offset, 9) == "<![CDATA[")
                                content.Append(ParseCDATA(xml, ref offset));
                            else if (offset + 4 < xml.Length && xml.Substring(offset, 4) == "<!--")
                                ParseComment(xml, ref offset);
                            else
                                ParseExclamination(xml, ref offset);
                        }
                        else if (cn == T_SLASH)
                        {
                            if (xml.Substring(offset + 2, tag.Length) == tag.ToString())
                            {
                                ret.Content = content.ToString();
                                offset += 2 + tag.Length;
                                return ret;
                            }
                            else
                                throw new XmlParserException("Opening and closing tag mismatch!");
                        }
                        else
                            ret.AddChild(ParseElement(xml, ref offset, ret));
                        continue;
                    }
                    else
                        content.Append(c);
                }
            }
            throw new XmlParserException("No ending tag for node!");
        }
    }
}
