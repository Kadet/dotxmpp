﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Protocol.Sasl
{
    class Failure : XmlParser.Dom.Element
    {
        public bool Aborted
        {
            get { return HasChild("aborted"); }
            set
            {
                if (value == false)
                    RemoveChild("aborted");
                else
                    SetChild("aborted", "");
            }
        }

        public Failure() : base("failure", "", URI.Sasl) { }
    }
}
