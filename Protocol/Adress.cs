﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace DotXMPP.Protocol
{
    public class Address
    {
        public string Server { get; set; }

        public Address() 
        {
            Server = "";
        }

        public Address(string address)
        {
            if (Regex.IsMatch(address, @"[a-zA-Z\-\_\.]*\.[a-z]{2,3}$"))
                Server = address;
            else
                throw new ArgumentException("Given address is invalid.", "address");
        }

        public virtual override string ToString()
        {
            return Server;
        }
    }
}
