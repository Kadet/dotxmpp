﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotXMPP.Protocol.Sasl
{
    class Auth : XmlParser.Dom.Element
    {
        public string Mechanism
        {
            get { return GetAttribute("mechanism"); }
            set { SetAttribute("mechanism", value.ToUpper()); }
        }

        public Auth(string mechanism, string content = "") : base("auth", content, URI.Sasl) 
        {
            Mechanism = mechanism;
        }
    }
}
